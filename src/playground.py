import pandas as pd
from kg_maker import KgMaker
from dotenv import load_dotenv, find_dotenv
import os
from openai import OpenAI
from loguru import logger
import json
from networkx.readwrite import json_graph
from langchain_community.embeddings.sentence_transformer import SentenceTransformerEmbeddings
from langchain_openai import ChatOpenAI, OpenAIEmbeddings
import networkx as nx
import matplotlib.pyplot as plt
from eventlog_handler import Event_handler
from langchain_text_splitters import RecursiveJsonSplitter
from langchain_community.vectorstores import Chroma
from langchain.prompts import ChatPromptTemplate
from langchain_core.output_parsers import StrOutputParser, JsonOutputParser
from langchain_core.runnables import RunnableLambda, RunnablePassthrough
from langchain_community.vectorstores import FAISS
from langchain_openai import ChatOpenAI
from langchain_core.prompts import PromptTemplate

class Document:
    def __init__(self, page_content, metadata=None):
        self.page_content = page_content
        self.metadata = metadata if metadata else {}
        
def format_docs(docs):
    return "\n\n".join(doc.page_content for doc in docs)

handler = Event_handler("data\payment_process_vet.xes")

# if handler.check_file_type():
  #   event_log = handler.read_file()
# else:
  #   print("Not supported")
    
# print(handler.list_of_columns())
main_columns = handler.select_columns(case_id="case:concept:name", timestamp="time:timestamp",activity="concept:name")
df = pd.read_csv("data/df.csv")
# df.to_csv("data/df.csv")


kg_maker = KgMaker(df=df,main_columns_dict=main_columns)
metrics = kg_maker.calculate_metrics()
meta_data = kg_maker.group_meta_data()
graph = kg_maker.df_to_kg(meta_data, metrics)

data = json_graph.node_link_data(graph)
json_data = json.dumps(data, indent=2)

logger.info(f"connect to OpenAI")
load_dotenv(find_dotenv())
api_key = os.getenv("OPENAI_API_KEY")
client = ChatOpenAI(api_key=api_key,model="gpt-4o",
    temperature=0,
    max_tokens=None,
    timeout=None,
    max_retries=2,)
embedding_model = OpenAIEmbeddings(model="text-embedding-3-large", api_key=api_key)
logger.success("Connected to OpenAI with Key")

#Prompting
template = """
You are an intelligent Process Mining assistant. You will receive context that is parts of a knowledge graph in JSON format. Your task is to answer the provided question based on this context and return a JSON that can be converted into a networkx knowledge graph.
Answer the question based only on the following context:
{context}

*Instructions:** 
- Your answer should be in JSON format.
- Provide two parts in the answer:
  1. The relevant knowledge graph with only the necessary nodes and links.
  2. A human-understandable answer explaining the result.
  
  Please give the output like this:
  {{
  "knowledge_graph": {{
    "directed": true,
    "multigraph": false,
    "nodes": [
      {{
        "id": "order"
      }},
      {{
        "id": "recieve"
      }}
    ],
    "links": [
      {{
        "Average Time": 20,
        "Frequency": 3,
        "source": "order",
        "target": "recieve"
      }}
    ]
  }},
  "human_answer": "The average time to receive an item is 20 hours."
}}
  
  

Question: {question}
"""
custom_rag_prompt = PromptTemplate.from_template(template)
question = """
how often does the vet send for credit collection?
"""
# Splitting
splitter = RecursiveJsonSplitter(max_chunk_size=300)
json_chunks = splitter.split_json(json_data=data, convert_lists = True)
documents = [Document(page_content=json.dumps(chunk)) for chunk in json_chunks]
# Storing and embedding
db = FAISS.from_documents(documents, embedding_model)
retriever = db.as_retriever(search_type="similarity", search_kwargs={"k": 6})

# Retrieving
retrieved_docs = retriever.invoke(question)

# Chaining
chain = (
    {"context": retriever | format_docs, "question": RunnablePassthrough()}
    | custom_rag_prompt
    | client
    | JsonOutputParser()
)

answer = chain.invoke(question)

# Answer Extraction
knowledge_graph = answer['knowledge_graph']
human_answer = answer['human_answer']




# Final Knowledge Graph

G = nx.DiGraph()

for node in knowledge_graph['nodes']:
    G.add_node(node['id'], **node)

for link in knowledge_graph['links']:
    G.add_edge(link['source'], link['target'], **link)

print(human_answer)
kg_maker.visualize_knowledge_graph(G)


    