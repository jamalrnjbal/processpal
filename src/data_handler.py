from eventlog_handler import Event_handler 
from kg_maker import KgMaker
from kg_to_llm import KgToLLM
import networkx as nx
import json
import pyzstd as zstd
import os
from fastapi import UploadFile

class Data_handler:
    event_handlers={}
    kgs={}
    llm_clients={}
    index_path="index.zst"
    chats={}
    root_path=""
    def __init__(self,root_path:str=None)-> None:
        self.root_path=root_path
        self.index_path=root_path+"/index.zst"
        print(self.index_path)
        try:
            tmp_file=zstd.open(self.index_path,"rt")
            for line in tmp_file:
                index_line=line.split("\t")
                self.kgs[index_line[0]]=nx.node_link_graph(json.loads(index_line[1]))
            print(self.kgs)
            tmp_file.close()
            for key in self.kgs.keys():
                self.chats[key]=[]
                try:
                    f=zstd.open(self.root_path+"/"+key+"-chat.zst","rt")
                    for line in f.readlines():
                        data=line.split("\t")
                        self.chats[key].append({"question":data[0],"answer":data[1],"graph":json.parse(data[2])})
                except:
                    print("No chat found")
        except Exception as e: print(e)

    def get_files():
        return self.index.keys()

    def get_kg_as_json(self,name:str):
        return json.dumps(nx.node_link_data(self.kgs[name]))

    def upload_file(self,name: str, file:UploadFile,do_cleanup:bool):
        eh=Event_handler(file)
        if not eh.check_file_type():
            return 1
        eh.read_file()
        if do_cleanup:
            eh.clean_data()
        self.event_handlers[name]=eh
    
    def select_columns(self, name:str, case_id:str, timestamp:str, activity:str, resource :str = None):
        df=self.event_handlers[name].return_dataframe()
        dic=self.event_handlers[name].select_columns(case_id, timestamp, activity, resource)
        kg=KgMaker(df,dic)
        metrics=kg.calculate_metrics()
        meta=kg.group_meta_data()
        graph=kg.df_to_kg(meta,metrics)
        #KgMaker.visualize_knowledge_graph(graph)
        self.kgs[name]=graph
        self.chats[name]=[]
        tmp_file=zstd.open(self.index_path,"at")
        tmp_file.write(name + "\t" + json.dumps(nx.node_link_data(graph))+ "\n")
        tmp_file.close()
        del self.event_handlers[name]

    def init_llm_client(self,name:str):
        self.llm_clients[name]=KgToLLM(self.kgs[name])
    def ask_question(self,name:str,query:str):
        answer = self.llm_clients[name].ask_question(query)
        try:
            tmp_file=zstd.open(self.root_path+"/"+name+"-chat.zst","at")
            tmp_file.write(f'{query}\t{answer["human_answer"]}\t{json.dumps(answer["knowledge_graph"])}\n')
            tmp_file.close()
        except:
            print("No chat found")
        if not name in self.chats:
            self.chats[name]=[]
        self.chats[name].append({"question":query,"answer":answer["human_answer"],"graph":answer["knowledge_graph"]})
        return answer

    def delete_knowledge_graphs(self,toDelete:list):
        for name in toDelete:
            del self.kgs[name]
            self.delete_chat_history(name)
        tmp_file=zstd.open(self.index_path,"rt")
        new_file = tmp_file.readlines()
        tmp_file.seek(0)
        for line in new_file:
            if not any(map(line.__contains__, toDelete)):
                tmp_file.write(line)
        tmp_file.truncate()
        tmp_file.close()
        return {"status": "OK"}
        
    def delete_chat_history(self,name:str):
        self.chats[name]=[]
        os.remove(self.root_path+"/"+name+"-chat.zst")