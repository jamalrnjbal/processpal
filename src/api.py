
from fastapi import FastAPI, File, UploadFile, WebSocket
from fastapi.responses import HTMLResponse
from fastapi.middleware.cors import CORSMiddleware
from eventlog_handler import Event_handler 
from data_handler import Data_handler
import json
import os
from pydantic.v1 import BaseModel
import shutil
from pathlib import Path
from tempfile import NamedTemporaryFile

class Item(BaseModel):
    toDelete:list

root_path:str = "/app/data"
#if not os.path.exists(tmp_path):
#    os.makedirs(tmp_path)
#for file in os.listdir(tmp_path):
#    os.remove(tmp_path+"/"+file)

data_handler:Data_handler = Data_handler(root_path)

app = FastAPI()

origins = [
    "*",
    "http://frontend:80",
    "http://localhost:80",
    "http://localhost:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
async def root():
    return os.getcwd()
    #return {"message": "RUNNING"}

@app.get("/api/supported_filetypes")
async def supported_filetypes():
    return {"filetypes":Event_handler.supported_filetypes()}

@app.get("/api/columns/{name}")
async def get_columns(name: str):
    return {"columns":data_handler.event_handlers[name].list_of_columns()}

@app.post("/api/upload_file/{name}")
async def upload_file(name:str,cleanup: bool,file: UploadFile):
    try:
        suffix = Path(file.filename).suffix
        with NamedTemporaryFile(delete=False, suffix=suffix) as tmp:
            shutil.copyfileobj(file.file, tmp)
            tmp_path = Path(tmp.name)
    finally:
        file.file.close()
    print(tmp_path)
    data_handler.upload_file(name,str(tmp_path),cleanup)
    return {"status":"OK"}

@app.post("/api/select_columns/{name}")
async def select_columns(name:str,case_id: str,timestamp:str,activity:str,resource:str):
    graph=data_handler.select_columns(name,case_id,timestamp,activity,resource)
    return {"status":"OK"}

@app.get("/api/knowledge_graph/{name}")
async def get_knowledge_graph(name:str):
    return data_handler.get_kg_as_json(name)

@app.get("/api/stored_knowledge_graphs")
async def stored_knowledge_graphs():
    return {"knowledge_graphs":list(data_handler.kgs.keys())}

@app.post("/api/delete_knowledge_graphs")
async def delete_knowledge_graphs(item:Item):
    return data_handler.delete_knowledge_graphs(item.toDelete)

@app.websocket("/ws/chat/{name}")
async def chat(name: str,websocket: WebSocket):
    await websocket.accept()
    data_handler.init_llm_client(name)
    while True:
        data = await websocket.receive_json()
        answer = data_handler.ask_question(name,data['message'])
        knowledge_graph = answer['knowledge_graph']
        human_answer = answer['human_answer']
        await websocket.send_json({"response":human_answer,"graph":json.dumps(knowledge_graph)})

@app.get("/api/chat_history/{name}")
async def get_chat_history(name:str):
    return {"history":json.dumps(data_handler.chats[name])}

@app.delete("/api/chat_history/{name}")
async def get_chat_history(name:str):
    data_handler.delete_chat_history(name)
    return {"status":"OK"}
    
if __name__ == "__main__":
    run()