import pandas as pd
from loguru import logger
from collections import defaultdict
import networkx as nx
import pandas as pd
#import matplotlib.pyplot as plt
from collections import Counter


class KgMaker:
    
    def __init__(self, df: pd.DataFrame, main_columns_dict: dict) -> None:
        
        self.df = df
        print(df.head())
        self.main_columns_dict = main_columns_dict

        self.df[main_columns_dict['timestamp']] = pd.to_datetime(df[main_columns_dict['timestamp']])
        
    def calculate_metrics(self) -> pd.DataFrame:
        """ Takes a dataframe (eventlog) and creates a dataframe with source and
        Target columns, frequency and average time

        Returns:
            DataFrame
        """
        
        timestamp = self.main_columns_dict["timestamp"]
        activity= self.main_columns_dict["activity"]
        case_id = self.main_columns_dict["case_id"]
        self.df = self.df.sort_values(by=[case_id, timestamp])

        transitions = defaultdict(int)
        times = defaultdict(list)

        grouped = self.df.groupby(case_id)

        for case_id, group in grouped:
            activities = list(group[activity])
            timestamps = list(group[timestamp])
            for i in range(len(activities) - 1):
                transition = (activities[i], activities[i + 1])
                transitions[transition] += 1
                time_diff = (timestamps[i + 1] - timestamps[i]).total_seconds() / 3600  # Convert to hours
                times[transition].append(time_diff)
  
        average_times = {transition: sum(times_list) / len(times_list) for transition, times_list in times.items()}

        transitions_df = pd.DataFrame(transitions.items(), columns=['Transition', 'Frequency'])
        average_times_df = pd.DataFrame(average_times.items(), columns=['Transition', 'Average Time'])
        average_times_df['Average Time'] = average_times_df['Average Time'].astype(int)

        transitions_df[['Source', 'Target']] = pd.DataFrame(transitions_df['Transition'].tolist(), index=transitions_df.index)
        average_times_df[['Source', 'Target']] = pd.DataFrame(average_times_df['Transition'].tolist(), index=average_times_df.index)
        average_times_df = average_times_df.drop(columns=['Transition'])
        transitions_df = transitions_df.drop(columns=['Transition'])
        transitions_df = transitions_df.merge(average_times_df, on=['Source', 'Target'], how='left')
        
        return transitions_df
    
    def group_meta_data(self)->pd.DataFrame:
        """Groups meta data by activity
        returns: Dataframe
        """
        timestamp = self.main_columns_dict["timestamp"]
        activity= self.main_columns_dict["activity"]
        case_id = self.main_columns_dict["case_id"]

        def custom_agg(x):
            if pd.api.types.is_numeric_dtype(x):
                return x.mean()
            elif pd.api.types.is_string_dtype(x):
                most_common = Counter(x).most_common(3)
                return [item for item, count in most_common]
            else:
                return list(x)
    
        grouped_df = self.df.drop(columns=[timestamp,case_id]).groupby(activity).agg(custom_agg).reset_index()
        return grouped_df

    def df_to_kg(self,attributes_df, graph_df) -> nx:
        """
    Create a knowledge graph from given dataframes and return the graph object.
    
    Parameters:
    - time_test_df: DataFrame containing Source, Target, Frequency, and Average Time
    - attributes_df: DataFrame containing Activity and other attributes
    
    Returns:
    - G: NetworkX graph object
    """
        activity= self.main_columns_dict["activity"]
        G = nx.DiGraph()

        # Add nodes
        for index, row in attributes_df.iterrows():
            G.add_node(row[activity])

        # Add edges with attributes
        for index, row in graph_df.iterrows():
            G.add_edge(row['Source'], row['Target'], frequency=row['Frequency'], average_time=row['Average Time'])
            
        return G
    
"""     @staticmethod
    def visualize_knowledge_graph(G) -> None:
        
    Visualize the given knowledge graph.
    
    Parameters:
    - G: NetworkX graph object
    
        pos = nx.spring_layout(G)
        edge_labels = {(u, v): f"Freq: {d['frequency']}, Avg Time: {d['average_time']}" for u, v, d in G.edges(data=True)}
        nx.draw(G, pos, with_labels=True, node_color='lightblue', node_size=3000, font_size=10, font_weight='bold', arrows=True)
        nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, font_color='red')

        plt.show() """
