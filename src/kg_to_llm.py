#from langchain_text_splitters import RecursiveJsonSplitter,CharacterTextSplitter
from networkx.readwrite import json_graph
#from langchain_community.vectorstores import FAISS
from langchain_openai import OpenAIEmbeddings
from dotenv import load_dotenv, find_dotenv
import os
from openai import OpenAI
from loguru import logger
import json
import pandas as pd
from kg_maker import KgMaker
from dotenv import load_dotenv, find_dotenv
import os
from openai import OpenAI
from loguru import logger
import json
from networkx.readwrite import json_graph
from langchain_community.embeddings.sentence_transformer import SentenceTransformerEmbeddings
from langchain_openai import ChatOpenAI, OpenAIEmbeddings
import networkx as nx
import matplotlib.pyplot as plt
from eventlog_handler import Event_handler
from langchain_text_splitters import RecursiveJsonSplitter
from langchain_community.vectorstores import Chroma
from langchain.prompts import ChatPromptTemplate
from langchain_core.output_parsers import StrOutputParser, JsonOutputParser
from langchain_core.runnables import RunnableLambda, RunnablePassthrough
from langchain_community.vectorstores import FAISS
from langchain_openai import ChatOpenAI
from langchain_core.prompts import PromptTemplate

class KgToLLM():
    def __init__(self, question):
        template = """
        You are an intelligent Process Mining assistant. You will receive context that is parts of a knowledge graph in JSON format. Your task is to answer the provided question based on this context and return a JSON that can be converted into a networkx knowledge graph.
        Answer the question based only on the following context:
        {context}

        *Instructions:** 
        - Your answer should be in JSON format.
        - Provide two parts in the answer:
        1. The relevant knowledge graph with only the necessary nodes and links.
        2. A human-understandable answer explaining the result.
        
        Please give the output like this:
        {{
        "knowledge_graph": {{
            "directed": true,
            "multigraph": false,
            "nodes": [
            {{
                "id": "order"
            }},
            {{
                "id": "recieve"
            }}
            ],
            "links": [
            {{
                "Average Time": 20,
                "Frequency": 3,
                "source": "order",
                "target": "recieve"
            }}
            ]
        }},
        "human_answer": "The average time to receive an item is 20 hours."
        }}
        Question: {question}
        """
        custom_rag_prompt = PromptTemplate.from_template(template)
        
        logger.info(f"connect to OpenAI")
        load_dotenv(find_dotenv())
        api_key = os.getenv("OPENAI_API_KEY")
        client = ChatOpenAI(api_key=api_key,model="gpt-4o",
            temperature=0,
            max_tokens=None,
            timeout=None,
            max_retries=2,)
        embedding_model = OpenAIEmbeddings(model="text-embedding-3-large", api_key=api_key)
        logger.success("Connected to OpenAI with Key")
        
    @staticmethod
    def __format_docs(docs):
        return "\n\n".join(doc.page_content for doc in docs)

    @staticmethod
    def split(graph):
        splitter = RecursiveJsonSplitter(max_chunk_size=300)
        json_chunks = splitter.split_json(json_data=graph, convert_lists = True)
        sub_graphs = [Document(page_content=json.dumps(chunk)) for chunk in json_chunks]
        
        return sub_graphs
        
    def store_embedd(self, sub_graphs):
        db = FAISS.from_documents(sub_graphs, self.embedding_model)
        self.retriever = db.as_retriever(search_type="similarity", search_kwargs={"k": 6})
        embedded_sub_graphs = self.retriever.invoke(self.question)
        
        return embedded_sub_graphs
        
    
    def llm(self):
        chain = (
            {"context": self.retriever | self.format_docs, "question": RunnablePassthrough()}
            | self.custom_rag_prompt
            | self.client
            | JsonOutputParser()
        )

        answer = chain.invoke(self.question)

        knowledge_graph = answer['knowledge_graph']
        human_answer = answer['human_answer']
        
        G = nx.DiGraph()
        
        return knowledge_graph, human_answer

class Document:
    def __init__(self, page_content, metadata=None):
        self.page_content = page_content
        self.metadata = metadata if metadata else {}
