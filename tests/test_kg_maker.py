import unittest
import pandas as pd
import sys
import os
import networkx as nx
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from src.kg_maker import KgMaker


class TestCalculateMetrics(unittest.TestCase):
    
    @classmethod
    def setUpClass(self):
        
        df = {
            'case_id': [1001, 1001, 1002, 1002, 1002, 1003, 1003],
            'activity': ['order', 'cancel', 'order', 'recieve', 'pay', 'order', 'cancel'],
            'timestamp': [
                            '2021-01-01 10:00 AM', '2021-01-01 02:00 PM', '2021-01-01 02:00 PM', '2021-01-02 08:00 AM',
                            '2021-01-02 10:00 AM', '2021-01-02 12:00 PM', '2021-01-02 02:00 PM'
                            ]
        }
        df = pd.DataFrame(df)
        df['timestamp'] = pd.to_datetime(df['timestamp'])
        
        main_columns_dict = {'case_id': 'case_id', 'activity': 'activity', 'timestamp': 'timestamp'}
        
        self.kg_maker = KgMaker(df=df, main_columns_dict=main_columns_dict)
        
    def test_frequency(self):
        
        # Arrange
        freq_test_df = {
            'Source': ['order','order', 'recieve'],
            'Target': ['cancel', 'recieve', 'pay'],
            'Frequency': [2, 1, 1],
            'Average Time': ['whatever', 'whatever', 'whatever']
        }
        freq_test_df = pd.DataFrame(freq_test_df)
        
        # Act 
        result_df = self.kg_maker.calculate_metrics()
        
        # Assert
        self.assertTrue(result_df['Frequency'].equals(freq_test_df['Frequency']), "Frequency error")
        
    
    def test_avg_team(self):
        
        # Arrange
        time_test_df = {
            'Source': ['order','order', 'recieve'],
            'Target': ['cancel', 'recieve', 'pay'],
            'Frequency': [2, 1, 1],
            'Average Time': [3.0, 18.0, 2.0]
        }
        time_test_df = pd.DataFrame(time_test_df)
        
        # Act 
        result_df = self.kg_maker.calculate_metrics()
        
        self.assertTrue(result_df['Average Time'].equals(time_test_df['Average Time']), "Average Time error")
        
    def test_number_of_rows(self):
        
        # Arrange 
        rows_test_df = {
            'Source': ['order','order', 'recieve'],
            'Target': ['cancel', 'recieve', 'pay'],
            'Frequency': [2, 1, 1],
            'Average Time': [3.0, 18.0, 2.0]
        }
        rows_test_df = pd.DataFrame(rows_test_df)
        
        # Act
        result_df = self.kg_maker.calculate_metrics()
        
        self.assertEqual(result_df.shape, rows_test_df.shape, "Source and Target activities error")
        
        
class TestGroupMetadata(unittest.TestCase):

    def setUp(self) -> None:
        self.df_same=pd.DataFrame({
            "case id":[1,2,2],
            "activity":["activity1","activity2","activity2"],
            "timestamp":["2024-05-01","2024-05-02","2024-05-03"],
            "meta1":["meta1_value1","meta1_value2","meta1_value3"],
            "meta2": ["meta2_value4", "meta2_value5", "meta2_value6"]
        })#have the same activity value
        self.meta_data_same=KgMaker(self.df_same,{})
    

        self.df=self.df_same=pd.DataFrame({
            "case id":[1,2,3],
            "activity":["activity1","activity2","activity3"],
            "timestamp":["2024-05-01","2024-05-02","2024-05-03"],
            "meta1":["meta1_value1","meta1_value2","meta1_value3"],
            "meta2": ["meta2_value4", "meta2_value5", "meta2_value6"]
        })#no same group
        self.meta_data=KgMaker(self.df,{})

    def test_add_meta_data(self):
        result_df_same=self.meta_data_same.group_meta_data()
        expected_df_same = pd.DataFrame({
            "activity": ["activity1", "activity2"],
            "meta1": ["meta1_value1", "meta1_value2"],
            "meta2": ["meta2_value4", "meta2_value5"]
        })

        result_df=self.meta_data.group_meta_data()
        expected_df=pd.DataFrame({
            "activity":["activity1","activity2","activity3"],
            "meta1":["meta1_value1","meta1_value2","meta1_value3"],
            "meta2": ["meta2_value4", "meta2_value5", "meta2_value6"]
        })
    
        pd.testing.assert_frame_equal(result_df_same,expected_df_same)
        pd.testing.assert_frame_equal(result_df, expected_df)
        
class TestDfToKg(unittest.TestCase):
    def setUp(self):
        
        self.attributes_df = pd.DataFrame({
            'Activity': ['A', 'B', 'C'],
            'Attr1': [1, 2, 3],
            'Attr2': ['x', 'y', 'z']
        })

        self.graph_df = pd.DataFrame({
            'Source': ['A', 'B', 'C'],
            'Target': ['B', 'C', 'A'],
            'Average Time': [10, 20, 30],
            'Frequency': [1, 3, 2]
        })

    def test_df_to_kg(self):
        
        G = KgMaker.df_to_kg(self.attributes_df, self.graph_df)
        
        self.assertIsInstance(G, nx.DiGraph, "Wrong Graph Type")

        for _, row in self.attributes_df.iterrows():
            activity = row['Activity']
            attributes = row.drop('Activity').to_dict()
            self.assertTrue(G.has_node(activity))
            self.assertEqual(G.nodes[activity], attributes)

        for _, row in self.graph_df.iterrows():
            source = row['Source']
            target = row['Target']
            attributes = row.drop(['Source', 'Target']).to_dict()
            self.assertTrue(G.has_edge(source, target))
            self.assertEqual(G.edges[source, target], attributes)

if __name__ == '__main__':
    unittest.main()
