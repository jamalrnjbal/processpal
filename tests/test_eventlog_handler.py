import unittest
from src.eventlog_handler import Event_handler
import pandas as pd

class Test_EventHandler(unittest.TestCase):
    def setUp(self) -> None:
        self.csv_file_path="test_data.csv"
        self.xls_file_path="test_data.xlsx"
        self.xes_file_path="test_data.xes"
        self.parquet_file_path="test_data.parquet"
        self.xlsx_file_path="test_data.xlsx"
        self.doc_file_path="test_data.doc"
        #all type file


    #test all type file
    def test_check_file_type(self):
        handler_csv=Event_handler(self.csv_file_path)
        result_csv=handler_csv.check_file_type()
        handler_xls=Event_handler(self.xls_file_path)
        result_xls=handler_xls.check_file_type()
        handler_xes=Event_handler(self.xes_file_path)
        result_xes=handler_xes.check_file_type()
        handler_parquet=Event_handler(self.parquet_file_path)
        result_parquet=handler_parquet.check_file_type()
        handler_xlsx=Event_handler(self.xlsx_file_path)
        result_xlsx=handler_xlsx.check_file_type()
        handler_doc=Event_handler(self.doc_file_path)
        result_doc=handler_doc.check_file_type()
        self.assertTrue(result_csv and result_parquet and result_xes and result_xls and result_xlsx)
        self.assertFalse(result_doc)

    def test_read_file(self)->None:
        handler_csv=Event_handler(self.csv_file_path)
        handler_csv.read_file()
        self.assertIsInstance(handler_csv.df, pd.DataFrame)
        handler_xls=Event_handler(self.xls_file_path)
        handler_xls.read_file()
        self.assertIsInstance(handler_xls.df, pd.DataFrame)
        handler_xes=Event_handler(self.xes_file_path)
        handler_xes.read_file()
        self.assertIsInstance(handler_xes.df, pd.DataFrame)
        handler_parquet=Event_handler(self.parquet_file_path)
        handler_parquet.read_file()
        self.assertIsInstance(handler_parquet.df, pd.DataFrame)
        handler_xlsx=Event_handler(self.xlsx_file_path)
        handler_xlsx.read_file()
        self.assertIsInstance(handler_xlsx.df, pd.DataFrame)

    def test_list_of_columns(self)->None:
        handler=Event_handler(self.csv_file_path)
        handler.check_file_type()
        handler.read_file()
        columns=handler.list_of_columns()
        self.assertIsInstance(columns,list)

    def test_select_columns(self)->None:
        handler = Event_handler(self.csv_file_path)
        handler.check_file_type()
        handler.read_file()
        selected_columns = handler.select_columns("caseid", "timestamp", "activity", "resource")
        self.assertIsInstance(selected_columns,dict)

    def test_timestamp_handler(self)->None:
       handler = Event_handler(self.csv_file_path)
       handler.check_file_type()
       handler.read_file()
       selected_columns = handler.select_columns("caseid", "timestamp", "activity", "resource")
       handler.timestamp_handler()
       expected_format=handler.df["timestamp"].dt.strftime('%Y-%m-%dT%H:%M:%S')
       actual_format = pd.to_datetime(handler.df['timestamp'], format='%Y-%m-%dT%H:%M:%S')
       self.assertTrue(all(expected_format == actual_format), "Date time format is not as expected.\
                        Expected: {} Actual: {}".format(expected_format, actual_format))

    def test_clean_data(self)->None:
        handler = Event_handler(self.csv_file_path)
        handler.check_file_type()
        handler.read_file()
        handler.clean_data()
        self.assertFalse(handler.df.isnull().any().any())

    def test_return_dataframe(self)->None:
        handler = Event_handler(self.csv_file_path)
        handler.check_file_type()
        handler.read_file()
        df = handler.return_dataframe()
        self.assertIsNotNone(df)
        self.assertTrue(df.equals(handler.df))

   
    def tearDown(self)->None:
        pass

if __name__=="__main__":
    unittest.main()