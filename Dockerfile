

FROM node:alpine AS build-ui

WORKDIR /usr/src/app

RUN npm cache clean --force

COPY ./ui/ ./

RUN npm install

RUN npm run build --production




FROM unit:python3.12

WORKDIR /app

COPY ./src/requirements.txt .


#RUN pip install pyg-lib -f https://data.pyg.org/whl/torch-2.3.0+cu118.html
RUN pip install --no-cache-dir --upgrade -r ./requirements.txt

COPY ./src/*.py /app/src/

COPY ./config.json /docker-entrypoint.d/config.json

COPY --from=build-ui /usr/src/app/dist/ui/browser/ /usr/share/nginx/html/
#COPY ./nginx.conf /etc/nginx/conf.d/default.conf

USER root

RUN mkdir /app/data && chmod -R 777 /app/data

VOLUME /app/data

EXPOSE 80
EXPOSE 8000
