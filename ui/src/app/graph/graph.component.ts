import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { SidebarService } from '../sidebar.service';
import { StateService } from '../state.service';
import Graph from "graphology";
import {Sigma} from "sigma";
import WebGLRenderer from "sigma";
import { DEFAULT_EDGE_CURVATURE, EdgeCurvedArrowProgram } from "@sigma/edge-curve";
import noverlap from 'graphology-layout-noverlap';
import circular from 'graphology-layout/circular';
import { EdgeArrowProgram } from 'sigma/rendering';
import { EdgeDisplayData, NodeDisplayData } from "sigma/types";

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrl: './graph.component.css'
})
export class GraphComponent{

  @ViewChild("Graph") container: ElementRef | null = null;
  graph: Graph = new Graph();
  state: { type: "idle" } | { type: "hovered"; edge: string; source: string; target: string } = { type: "idle" };
  sigma?: Sigma;

  constructor(public sidebarService: SidebarService,public stateService: StateService) { 
  }

  recieve_answer(msg:any){
    console.log(msg);
    let response=JSON.parse(JSON.parse(JSON.stringify(msg)).graph);
    this.create_graph(response);
  }

  @ViewChild('Graph')
  set watch(div: ElementRef) {
    if(div) { 
      console.log("graph visible");
      this.stateService.load_kg().subscribe(result => {
        var res = JSON.stringify(result).replace(/NaN/gi, "null");
        console.log(res);
        let data=JSON.parse(JSON.parse(res));
        this.create_graph(data);
      });
      this.stateService.subject.subscribe({
        next: (msg: any) => this.recieve_answer(msg),
        error: (err: any) => console.log(err),
        complete: () => console.log('complete')
       });
    }
  }

  getCurvature(index: number, maxIndex: number): number {
    if (maxIndex <= 0) throw new Error("Invalid maxIndex");
    if (index < 0) return -this.getCurvature(-index, maxIndex);
    const amplitude = 3.5;
    const maxCurvature = amplitude * (1 - Math.exp(-maxIndex / amplitude)) * DEFAULT_EDGE_CURVATURE;
    return (maxCurvature * index) / maxIndex;
  }

  create_graph(data:any){
    if(this.sigma){
      this.sigma.kill();
    }
    this.graph=new Graph();
    for(let node of data.nodes){
      this.graph.addNode(node.id,{size:15,label:node.id});
    }
    for(let link of data.links){
        this.graph.addEdge(link.source,link.target,{size:2*(1-(Math.E**(-link.frequency))), forceLabel:true,label:"Frequency: "+link.frequency+", Average Time: "+link.average_time, type: "curvedArrow"});
    }
    circular.assign(this.graph);
    noverlap.assign(this.graph,100);
    if(this.container){
      this.sigma = new WebGLRenderer(this.graph, this.container.nativeElement,
        {
        allowInvalidContainer: true,
        defaultEdgeType: "curved",
        enableEdgeEvents: true,
        renderEdgeLabels: true,
        edgeProgramClasses: {
          straight: EdgeArrowProgram,
          curvedArrow: EdgeCurvedArrowProgram,
        },
        edgeReducer: (edge, attributes) => {
          const res: Partial<EdgeDisplayData> = { ...attributes };
    
          if (this.state.type === "hovered") {
            if (edge === this.state.edge) {
              res.size = (res.size || 1) * 1.5;
              res.zIndex = 1;
            } else {
              res.color = "#f0f0f0";
              res.zIndex = 0;
            }
          }
    
          return res;
        },
        nodeReducer: (node, attributes) => {
          const res: Partial<NodeDisplayData> = { ...attributes };
    
          if (this.state.type === "hovered") {
            if (node === this.state.source || node === this.state.target) {
              res.highlighted = true;
              res.zIndex = 1;
            } else {
              res.label = undefined;
              res.zIndex = 0;
            }
          }
    
          return res;
        },
      });
      if(this.sigma){
      this.sigma.on("enterEdge", ({ edge }) => {
        this.state = { type: "hovered", edge, source: this.graph.source(edge), target: this.graph.target(edge) };
        if(this.sigma)this.sigma.refresh();
      });
      this.sigma.on("leaveEdge", () => {
        this.state = { type: "idle" };
        if(this.sigma)this.sigma.refresh();
      });
      }
    }
  }
}
