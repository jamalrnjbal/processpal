import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { webSocket } from 'rxjs/webSocket';


@Injectable({
  providedIn: 'root'
})


export class StateService {

  api_url="/api"
  name="";
  subject!: any;

  constructor(private http: HttpClient) {
  }

  open_socket(){
    this.subject = webSocket({url:'/ws/chat/'+this.name});
  }

  upload_file(file: File,cleanup:boolean){
    let formData: FormData = new FormData();
    formData.append('file', file, file.name);
    this.name.trim().length == 0 ? this.name = file.name.trim() : this.name=this.name.trim();
    return this.http.post<any>(this.api_url+'/upload_file/' + this.name, formData, { params: new HttpParams().set('cleanup', cleanup), responseType: 'json',reportProgress: true,observe: 'events' });
  }

  get_columns():Observable<any> {
    return this.http.get<any>(this.api_url+'/columns/' + this.name);
  }

  load_kg():Observable<any> {
    return this.http.get<any>(this.api_url+'/knowledge_graph/' + this.name);
  }

  select_columns(case_id:string,timestamp:string,activity:string) {
    return this.http.post<any>(this.api_url+'/select_columns/' + this.name, {}, { params: new HttpParams().set('case_id', case_id).set('timestamp', timestamp).set('activity', activity).set('resource', ''), responseType: 'json' });
  }

  get_supported_filetypes() {
    return this.http.get<any>(this.api_url+'/supported_filetypes', { responseType: 'json' });
  }

  get_stored_kgs() {
    return this.http.get<any>(this.api_url+'/stored_knowledge_graphs', { responseType: 'json' });
  }

  get_chat_history() {
    return this.http.get<any>(this.api_url+'/chat_history/'+this.name, { responseType: 'json' });
  }

  update_name(name:string){
    this.name=name;
  }

  delete_knowledge_graphs(toDelete:string[]){
    return this.http.post<any>(this.api_url+'/delete_knowledge_graphs', {"toDelete":toDelete}, { responseType: 'json' });
  }

  delete_chat_history(){
    return this.http.delete<any>(this.api_url+'/chat_history/'+this.name, { responseType: 'json' });
  }

}
