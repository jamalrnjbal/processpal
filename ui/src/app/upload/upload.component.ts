import { Component, Directive, ElementRef, ViewChild } from '@angular/core';
import { SidebarService } from '../sidebar.service';
import { StateService } from '../state.service';
import { Observable } from 'rxjs';
import { HttpEventType } from '@angular/common/http';



@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrl: './upload.component.css',
})
export class UploadComponent {

  caseid = "";
  timestamp = "";
  activity = "";
  fileName = "";
  file: File | undefined;
  fileTypeOK: boolean = false;
  metadataOK: boolean = false;
  name = "";
  cleanup = false;
  file_uploaded=false;
  uploadProgress:number=0;

  filetypes:string[]=[]
  columns:string[]=[]
  constructor(public sidebarService: SidebarService, public stateService: StateService) {}

  upload(){
    if(this.file!==undefined){
      this.stateService.name=this.name;
      this.name="";
      this.uploadProgress=0;
      this.stateService.upload_file(this.file,this.cleanup).subscribe(event => {
        switch (event.type){
          case HttpEventType.UploadProgress:
            if(event.total)
              this.uploadProgress=100*event.loaded/event.total
            break;
          case HttpEventType.Response:
            this.stateService.get_columns().subscribe(res=>this.columns=res.columns)
        }
      })
    }
    else{
      alert("No File Selected yet");
    }
  }

  @ViewChild('Upload') 
  set watch(div: ElementRef) {
    if(div) {
      this.stateService.get_supported_filetypes().subscribe(result => {
        this.filetypes=result.filetypes;
      });
    }
  }

  onFileUpload(event: any) {
    const file: File = event.target.files[0];
    if (this.filetypes.some(str => file.name.includes(str))) {
      this.fileTypeOK = true;
      this.fileName = file.name;
      this.file = file;
    }
    else{
      alert('Filetype not supported');
    }
  }
  validateMetadata() {
    if (this.caseid != "" && this.timestamp != "" && this.activity != "") {
      this.metadataOK = true;
    }
    else {
      this.metadataOK = false;
    }
  }

  select_columns(){
    this.stateService.select_columns(this.caseid,this.timestamp,this.activity).subscribe(result=>this.sidebarService.setViewState('graph'))
  }

}
