import { Component, ElementRef, ViewChild } from '@angular/core';
import { StateService } from '../state.service';
import { SidebarService } from '../sidebar.service';

@Component({
  selector: 'app-load',
  templateUrl: './load.component.html',
  styleUrl: './load.component.css'
})
export class LoadComponent {

  delete:boolean = false;
  kgs:string[]=[]
  delete_choice:{[key: string]: boolean}={};

  constructor(public stateService:StateService,public sidebarService:SidebarService){}
  @ViewChild('Load') 
  set watch(div: ElementRef) {
    if(div) {
      this.delete_choice={};
      this.stateService.get_stored_kgs().subscribe(result => {
        this.kgs=result.knowledge_graphs
      });
    }
  }

  confirm_deletion(){
    let toDelete:string[]=[];
    for(let key of this.kgs){
      if(this.delete_choice[key]){
        toDelete.push(key);
      }
    }
    if(toDelete.length===0){
      return;
    }
    if (confirm('Are you sure you want to delete the selected knowledge graphs?')) {
      this.stateService.delete_knowledge_graphs(toDelete).subscribe(res => {
        this.delete_choice={};
        this.stateService.get_stored_kgs().subscribe(result => {
        this.kgs=result.knowledge_graphs
      });
      });
    } else {
      for(let key of this.kgs){
        this.delete_choice[key]=false;
      }
    }
  }
}
