import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { SidebarService } from '../sidebar.service';
import { StateService } from '../state.service';


import Graph from "graphology";
import {Sigma} from "sigma";
import WebGLRenderer from "sigma";
import { DEFAULT_EDGE_CURVATURE, EdgeCurvedArrowProgram } from "@sigma/edge-curve";
import noverlap from 'graphology-layout-noverlap';
import circular from 'graphology-layout/circular';
import { EdgeArrowProgram } from 'sigma/rendering';
import { EdgeDisplayData, NodeDisplayData } from "sigma/types";
import { isFakeMousedownFromScreenReader } from '@angular/cdk/a11y';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})



export class ChatComponent implements OnInit {

  history:{[key: number]: any}={};

  @ViewChild("Graph") container: ElementRef | null = null;
  graph: Graph = new Graph();
  state: { type: "idle" } | { type: "hovered"; edge: string; source: string; target: string } = { type: "idle" };
  sigma?: Sigma;
  
  id_focus=-1;
  id_counter=0;
  message: string = "" ;
  messages: Message[] = [];

  constructor(public stateService: StateService, public sidebarService: SidebarService) {}

  ngOnInit() {
    this.id_counter=0;
    this.stateService.load_kg().subscribe(result => {
      var res = JSON.stringify(result).replace(/NaN/gi, "null");
      let data=JSON.parse(JSON.parse(res));
      this.history[-1]=data
      this.create_graph(data);
    });
    this.stateService.get_chat_history().subscribe(result=>{
      let res=JSON.parse(result.history);
      for(let line of res){
        this.history[this.id_counter]=line.graph;
        this.messages.push(new Message(this.id_counter,0,line.question));
        this.messages.push(new Message(this.id_counter++,1,line.answer));
      }
    });
    this.stateService.open_socket()
    this.stateService.subject.subscribe({
      next: (msg: any) => this.recieve_answer(msg), // Called whenever there is a message from the server.
      error: (err: any) => console.log(err), // Called if at any point WebSocket API signals some kind of error.
      complete: () => console.log('complete') // Called when connection is closed (for whatever reason).
     });
  }

  

  recieve_answer(msg:any){
    console.log(msg.graph)
    let graph=JSON.stringify(msg.graph).replace(/NaN/gi, "null");
    graph = JSON.parse(JSON.parse(graph));
    this.history[this.id_counter]=graph;
    this.messages.push(new Message(this.id_counter++,1,msg.response));
    this.create_graph(graph);
  }

  sendMessage() {
    this.messages.push(new Message(this.id_counter,0,this.message))
    //this.chatService.sendMessage(this.message);
    this.stateService.subject.next({'message':this.message});
    this.message = '';
  }

/*   getCurvature(index: number, maxIndex: number): number {
    if (maxIndex <= 0) throw new Error("Invalid maxIndex");
    if (index < 0) return -this.getCurvature(-index, maxIndex);
    const amplitude = 3.5;
    const maxCurvature = amplitude * (1 - Math.exp(-maxIndex / amplitude)) * DEFAULT_EDGE_CURVATURE;
    return (maxCurvature * index) / maxIndex;
  } */

  create_graph(data:any){
    console.log(data)
    if(this.sigma){
      this.graph.clear();
      this.sigma.kill();
    }
    this.graph=new Graph();
    for(let node of data.nodes){
      try{
        this.graph.addNode(node.id,{size:15,label:node.id});
      }
      catch{
      } 
    }
    for(let link of data.links){
      try{
        this.graph.addEdge(link.source,link.target,{size:2*(1-(Math.E**(-link.frequency))), forceLabel:true,label:"Frequency: "+link.frequency+", Average Time: "+link.average_time, type: "curvedArrow"});
      }
      catch{
        this.graph.mergeEdge(link.source,link.target,{label:"Frequency: "+link.frequency+", Average Time: "+link.average_time})
      }
    }
    circular.assign(this.graph);
    noverlap.assign(this.graph,100);
    if(this.container){
      this.sigma = new WebGLRenderer(this.graph, this.container.nativeElement,
        {
        allowInvalidContainer: true,
        defaultEdgeType: "curvedArrow",
        enableEdgeEvents: true,
        renderEdgeLabels: true,
        edgeProgramClasses: {
          straight: EdgeArrowProgram,
          curvedArrow: EdgeCurvedArrowProgram,
        },
        edgeReducer: (edge, attributes) => {
          const res: Partial<EdgeDisplayData> = { ...attributes };
    
          if (this.state.type === "hovered") {
            if (edge === this.state.edge) {
              res.size = (res.size || 1) * 1.5;
              res.zIndex = 1;
            } else {
              res.color = "#f0f0f0";
              res.zIndex = 0;
            }
          }
    
          return res;
        },
        nodeReducer: (node, attributes) => {
          const res: Partial<NodeDisplayData> = { ...attributes };
    
          if (this.state.type === "hovered") {
            if (node === this.state.source || node === this.state.target) {
              res.highlighted = true;
              res.zIndex = 1;
            } else {
              res.label = undefined;
              res.zIndex = 0;
            }
          }
    
          return res;
        },
      });
      if(this.sigma){
      this.sigma.on("enterEdge", ({ edge }) => {
        this.state = { type: "hovered", edge, source: this.graph.source(edge), target: this.graph.target(edge) };
        if(this.sigma)this.sigma.refresh();
      });
      this.sigma.on("leaveEdge", () => {
        this.state = { type: "idle" };
        if(this.sigma)this.sigma.refresh();
      });
      }
    }
  }

  deleteChatHistory(){
    if (confirm('Are you sure you want to permanently clear the chat history?')) {
      this.stateService.delete_chat_history().subscribe(res => {
        this.id_counter=0
        this.id_focus=-1
        let graph=this.history[-1];
        this.history={}
        this.history[-1]=graph
        this.messages=[]
        this.create_graph(this.history[-1])
      });
    }
  }

  toggleMessagesActive(id:number){
    for(let m of this.messages){
      if(m.id===id){
        m.active=true;
      }
      else{
        m.active=false;
      }
    }
  }

}

class Message{
  active:boolean=false;
  type: number;
  message: string;
  id: number;
  constructor(id: number,type: number, message: string){
    this.type=type;
    this.message=message;
    this.id=id;
  }
}