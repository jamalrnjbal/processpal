import { ElementRef, Injectable, ViewChild } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  viewState = "start";
  prevViewState = "load";

  setViewState(state:string){
    this.viewState=state;
  }

  toggleViewState(){
    let tmpViewState=this.viewState;
    this.viewState = this.prevViewState;
    this.prevViewState = tmpViewState;
  }

}
