import { Component } from '@angular/core';
import { SidebarService } from './sidebar.service';
import { StateService } from './state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {

  constructor(public sidebarService: SidebarService,public stateService:StateService) {}
  title = 'ui';
}
