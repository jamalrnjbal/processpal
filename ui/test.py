import pandas as pd
import pm4py

df = pd.DataFrame({
            "case_id":["1","1","2","2","3","3"],
            "activity":["order","payment","order","cancel","order","payment"],
            "timestamp":["2024-05-01","2024-05-02","2024-05-03","2024-05-04","2024-05-06","2024-05-08"],
            "cost":["0","3","0","6","0","3"],
            "article": ["mobile", "mobile", "headphone","headphone","pc","pc"]
        })

df.to_parquet('test.parquet')
dataframe = pm4py.format_dataframe(df, case_id='case_id', activity_key='activity', timestamp_key='timestamp')
event_log = pm4py.convert_to_event_log(df)
pm4py.write_xes(event_log, 'test.xes')

log = pm4py.read_xes('test.xes')
df2 = pm4py.convert_to_dataframe(log)
print(df2)